package archive

import (
	"time"
)

type YearMonth struct {
	Year int

	// time.Month is a 1-based month counting system
	Month time.Month
}

func CurrentYearMonth() YearMonth {
	return YearMonth{time.Now().Year(), time.Now().Month()}
}

func (ym YearMonth) Equals(other YearMonth) bool {
	return ym.Year == other.Year && ym.Month == other.Month
}

func (ym YearMonth) Next() YearMonth {
	if ym.Month == time.December {
		return YearMonth{Year: ym.Year + 1, Month: time.January}
	} else {
		return YearMonth{Year: ym.Year, Month: ym.Month + 1}
	}
}

func (ym YearMonth) Prev() YearMonth {
	if ym.Month == time.January {
		return YearMonth{Year: ym.Year - 1, Month: time.December}
	} else {
		return YearMonth{Year: ym.Year, Month: ym.Month - 1}
	}
}

// Index returns a single int that can be used to compare this YearMonth with
// other YearMonth objects.
func (ym YearMonth) Index() int {
	return (ym.Year * 12) + (int(ym.Month) - 1)
}
