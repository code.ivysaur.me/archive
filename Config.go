package archive

import (
	"time"
)

type LogLocation struct {

	// LogFilePath will be passed to time.Format().
	LogFilePath string

	StartMonth YearMonth

	// Leave null to use a current/ongoing log
	EndMonth *YearMonth
}

type LogSource struct {
	Description string

	// Short strings by which this source can be identified. If any slugs are
	// present, the first slug will be used as the canonical URL.
	Slugs []string

	// Disk location of logs for this source, for each partial date range
	FileLocation []LogLocation
}

func (this *LogSource) EarliestDate() YearMonth {
	// assume it's the first in the list (although it might not be)
	return this.FileLocation[0].StartMonth
}

func (this *LogSource) LatestDate() YearMonth {
	// assume it's the last in the list (although it might not be)
	end := this.FileLocation[len(this.FileLocation)-1].EndMonth
	if end == nil {
		return CurrentYearMonth()
	} else {
		return *end
	}
}

type Config struct {
	Timezone     string
	LinesPerPage int
	Logs         []LogSource
}

func NewConfig() *Config {
	return &Config{
		Timezone:     time.Local.String(),
		LinesPerPage: 100,
		Logs:         make([]LogSource, 0),
	}
}
