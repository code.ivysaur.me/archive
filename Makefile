#
# Makefile for archive
#

VERSION:=3.1.1

SOURCES:=Makefile \
	static \
	cmd $(wildcard cmd/archive-server/*.go) \
	$(wildcard *.go)

GOFLAGS := -ldflags='-s -w -X code.ivysaur.me/archive.SERVER_VERSION=archive/${VERSION}' -gcflags='-trimpath=$(GOPATH)' -asmflags='-trimpath=$(GOPATH)'

#
# Phony targets
#
	
.PHONY: all dist clean devel

all: build/linux64/archive-server build/win32/archive-server.exe

dist: \
	_dist/archive-$(VERSION)-linux64.tar.gz \
	_dist/archive-$(VERSION)-win32.7z \
	_dist/archive-$(VERSION)-src.zip

clean:
	if [ -f ./staticResources.go ] ; then rm ./staticResources.go ; fi
	if [ -d ./build ] ; then rm -r ./build ; fi
	if [ -f ./archive ] ; then rm ./archive ; fi

devel: staticResources.go
	( cd cmd/archive-server && go build && ./archive-server -listen "127.0.0.1:8000" )

#
# Generated files
#

staticResources.go: static/ static/*
	go-bindata -o staticResources.go -prefix static -pkg archive static
	

#
# Release artefacts
#
	
build/linux64/archive-server: $(SOURCES) staticResources.go
	mkdir -p build/linux64
	(cd cmd/archive-server ; \
		CGO_ENABLED=1 GOOS=linux GOARCH=amd64 \
		go build $(GOFLAGS) -o ../../build/linux64/archive-server \
	)
	
build/win32/archive-server.exe: $(SOURCES) staticResources.go
	mkdir -p build/win32
	(cd cmd/archive-server ; \
		PATH=/usr/lib/mxe/usr/bin:$(PATH) CC=i686-w64-mingw32.static-gcc \
		CGO_ENABLED=1 GOOS=windows GOARCH=386 \
		go build $(GOFLAGS) -o ../../build/win32/archive-server.exe \
	)

build/linux64/config.json.SAMPLE: cmd/archive-server/config.json.SAMPLE
	cp cmd/archive-server/config.json.SAMPLE build/linux64/config.json.SAMPLE

build/win32/config.json.SAMPLE: cmd/archive-server/config.json.SAMPLE
	cp cmd/archive-server/config.json.SAMPLE build/win32/config.json.SAMPLE
	
_dist/archive-$(VERSION)-linux64.tar.gz: build/linux64/archive-server build/linux64/config.json.SAMPLE
	mkdir -p _dist
	tar caf _dist/archive-$(VERSION)-linux64.tar.gz -C build/linux64 archive-server config.json.SAMPLE --owner=0 --group=0
	
_dist/archive-$(VERSION)-win32.7z: build/win32/archive-server.exe build/win32/config.json.SAMPLE
	mkdir -p _dist
	( cd build/win32 ; \
		if [ -f dist.7z ] ; then rm dist.7z ; fi ; \
		7z a dist.7z archive-server.exe config.json.SAMPLE ; \
		mv dist.7z ../../_dist/archive-$(VERSION)-win32.7z \
	)
	
_dist/archive-$(VERSION)-src.zip: $(SOURCES)
	 hg archive --type=zip _dist/archive-$(VERSION)-src.zip
	
