# archive

![](https://img.shields.io/badge/written%20in-Go-blue.svg)

A web interface for browsing chat logs.

As of the 3.0 release, `archive` is available as a standalone binary for Linux and Windows.

## Changelog

2017-12-10: 3.1.0
- Feature: Statistics page
- Enhancement: Upgrade corner menu image to support high DPI
- Enhancement: Mobile-friendly buttons in search results
- Enhancement: Explain keyboard shortcuts when hovering over form elements
- Enhancement: Improve performance when changing sources and when searching, by removing a network roundtrip
- Enhancement: "Latest" link takes you to the latest for the current data source
- Change ctrl+alt+[H/J/K/L/M]... keyboard shortcuts to just alt+[...], remove dependency on APL `mousetrap.js`
- Fix an issue with accessing the legacy php controller after the Go port
- Fix an issue with using plus characters in regular expression searches
- Fix an issue with blank pages appearing if there was a divisible number of log entries
- Fix an issue with browsers using stale stylesheets
- Fix a cosmetic issue with text selection when using dropdown menu items
- Fix a cosmetic issue with PCRE text in regular expression hover message (it's no longer PCRE)
- Fix a cosmetic issue with search box layout at some screen sizes
- Fix a cosmetic issue with animation responsiveness

2017-09-06: 3.0.1
- Breaking: Revert date formatting in filenames back to strftime-compatible

2017-08-13: 3.0.0
- Rewritten in Go.
- Enhancement: Standalone binary server
- Fix a cosmetic issue with non-uniform filenames in downloaded archives

2016-05-27: r102
- Fix an issue with months for log sources with multiple data sources
- Fix an issue with the next button when navigating away from an error page

2016-04-03: r93
- Feature: Support multiple slugs for each log source
- Feature: Support reading log sources from multiple local file paths
- Enhancement: Nicer page URLs
- Enhancement: Preserve last regex search when switching log sources
- Enhancement: Match more types of system message
- Use platform month translations
- Fix an issue with username regular expression matching leading `<` without trailing `>`
- Fix an issue exporting full archives on low-memory servers
- Fix an issue producing large search results on low-memory servers
- Fix an issue on servers with unclean include paths

2015-04-10: r56
- Feature: Highlight result row from search
- Change archive to passthru() tarball, reducing peak memory usage
- Fix not always loading most recent javascript
- Fix source selection not displaying on search page
- Fix error message on bad regex
- Fix insufficiently strict regex delimiter escaping
- Fix blank line on last page

2014-11-14: r24
- Feature: Keyboard shortcuts
- Feature: Download archive
- Feature: Change font size
- Enhancement: Clickable magnet links
- Fix an issue with reloading regex filters in Firefox
- Fix an issue with mobile CSS styles
- Fix right-right arrow to go to latest page instead of latest-at-time-of-pageload

2014-05-25: r9
- Initial public release

2011-04-27: r0-private
- Initial internal release (earliest known reference to active application)
