package archive

import (
	"errors"
	"fmt"
	"regexp"
	"time"

	"github.com/jehiah/go-strftime"
)

var (
	SERVER_VERSION = "archive/0.0.0"
)

type ArchiveServer struct {
	timezone *time.Location
	cfg      *Config
	startup  time.Time

	rxViewRoot, rxViewPage, rxSearch, rxSearchRx, rxStats *regexp.Regexp
}

func NewArchiveServer(cfg *Config) (*ArchiveServer, error) {
	tz, err := time.LoadLocation(cfg.Timezone)
	if err != nil {
		return nil, err
	}

	if cfg.LinesPerPage <= 0 {
		return nil, fmt.Errorf("Invalid %d lines per page", cfg.LinesPerPage)
	}

	if len(cfg.Logs) == 0 {
		return nil, fmt.Errorf("No log sources configured")
	}

	for _, ls := range cfg.Logs {
		if len(ls.FileLocation) == 0 {
			return nil, fmt.Errorf(`No file locations for log source "%s"`, ls.Description)
		}
	}

	return &ArchiveServer{
		timezone:   tz,
		cfg:        cfg,
		startup:    time.Now(),
		rxViewRoot: regexp.MustCompile(`^/([^/]+)/(\d+)/(\d+)$`),
		rxViewPage: regexp.MustCompile(`^/([^/]+)/(\d+)/(\d+)/(?:page-)?(\d+)$`),
		rxSearch:   regexp.MustCompile(`^/([^/]+)/search/(.*)$`),
		rxSearchRx: regexp.MustCompile(`^/([^/]+)/rx/(.*)$`),
		rxStats:    regexp.MustCompile((`^/([^/]+)/stats/?$`)),
	}, nil
}

var ErrNoLogForMonth = errors.New("No logs for the selected month.")

func (this *ArchiveServer) LogFile(ls *LogSource, ym YearMonth) (string, error) {
	ymIndex := ym.Index()

	for _, fl := range ls.FileLocation {
		if fl.StartMonth.Index() <= ymIndex && (fl.EndMonth == nil || fl.EndMonth.Index() >= ymIndex) {
			return strftime.Format(fl.LogFilePath, time.Date(ym.Year, ym.Month, 1, 0, 0, 0, 0, this.timezone)), nil
		}
	}

	return "", ErrNoLogForMonth
}
