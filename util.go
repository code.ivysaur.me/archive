package archive

import (
	"fmt"
	"net/http"
	"strconv"
)

func attr(condition bool, whenMet string) string {
	if condition {
		return whenMet
	} else {
		return ""
	}
}

type URLHelper struct {
	w http.ResponseWriter
	r *http.Request
}

func (this *URLHelper) intval(sz string) int {
	ret, _ := strconv.Atoi(sz)
	return ret
}

func (this *URLHelper) get(sz string) string {
	return this.r.URL.Query().Get(sz) // n.b. automatically unescaped
}

func (this *URLHelper) hasGet(sz string) bool {
	return len(this.r.URL.Query().Get(sz)) > 0
}

func (this *URLHelper) redirectf(format string, a ...interface{}) {
	http.Redirect(this.w, this.r, fmt.Sprintf(format, a...), http.StatusTemporaryRedirect)
}
