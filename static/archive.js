/* archive.js */ 

var alreadyLoaded = false;

var $chatArea = document.getElementById("chatarea");
var DEFAULT_FONT_SIZE = 12;

function cookie_set(key, value) {
	document.cookie = (key+"="+value+"; expires=Sat, 20 Sep 2059 09:05:12; path=/");
}

function cookie_clear(key) {
	document.cookie = (key+"=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/"); 
}

function cookie_get(key) {
	var parts = document.cookie.split("; ").map(function(x) { return x.split("="); });
	for (var i = 0, e = parts.length; i !== e; ++i) {
		if (parts[i][0] == key) {
			return parts[i][1];
		}
	}
	return null;
}

function highlight(str) {	
	var urldesc = function(s) {
		return decodeURIComponent(s.replace(/\+/g, " "));
	};
	
	return str	
		.replace(/(\[\d\d\d\d-\d\d-\d\d\s\d\d\:\d\d\:\d\d\] )(\*.+)/g, "$1<span class=\"sys\">$2</span>")
		.replace(
			/\[(\d\d\d\d-\d\d-\d\d)\s(\d\d\:\d\d)\:(\d\d)\]/g,
			'<span class="timestamp">[<span class="ts_split_d">$1 </span><span class="ts_split_hm">$2</span><span class="ts_split_s">:$3</span>]</span>'
		)
		.replace(/(\[[0-9:\-\s]*?\])/g, '<span class="timestamp">$1</span>')
		.replace(/(\&lt\;[^\s]+?\&gt\;)/g, "<span class=\"chat\">$1</span>")
		.replace(/(\*\*\*.+)/g, "<span class=\"sys\">$1</span>")
		.replace(/(\&gt\;imp[^\n\r\<]*)/g, "<span class=\"gt\">$1</span>")
		.replace(/(https?:\/\/.+?)([\s|<])/g, "<a href=\"$1\" rel=\"noreferrer\">$1</a>$2")
		.replace(/magnet:\?.+dn=([^\< ]+)/g, function(match, m1) {
			return "<a href=\"" + match + "\">[MAGNET] " + urldesc(m1) + "</a>";
		})
	;
}

function fontSize(change) {
	var curSize = cookie_get("fontsize");
	if (curSize === null) {
		curSize = DEFAULT_FONT_SIZE;
	} else {
		curSize = + curSize;
	}
	
	curSize += change;
	
	cookie_set("fontsize", curSize);
	
	$chatArea.style["fontSize"] = ""+curSize+"px";
}

function toggleMenu() {
	var $container = document.getElementById("menu-container");
	$container.style.display = ($container.style.display == 'block') ? 'none' : 'block';
}

function highlightLine(no) {
	var lines = $chatArea.innerHTML.split("\n");
	
	lines[no] = '<span class="line-highlighted">' + lines[no] + '</span>';
	
	$chatArea.innerHTML = lines.join("\n");
}

function onLoad() {
	
	if (alreadyLoaded) {
		return;
	}	
	alreadyLoaded = true;
	
	//
	
	$chatArea.innerHTML = highlight( $chatArea.innerHTML );
	
	//
	
	document.getElementById("logo").addEventListener("click", toggleMenu);
	
	//
	
	if (
		! /\/search\//.test(window.location.pathname) &&
		document.location.hash.substr(0, 6) === '#line-'
	) {
		highlightLine( parseInt(document.location.hash.substr(6), 10) );
	}
	
	//
	
	fontSize(0);
	
	//
	
	var $form = document.getElementById("search-form");
	$form.addEventListener("submit", function(ev) {
		var query = $form.elements["q"].value;
		if (query.length === 0) {
			alert("No search text entered");
			ev.preventDefault();
			return false;
		}
		
		var prefix = ($form.elements["rx"].checked ? "/rx/" : "/search/");
		window.location.hash = "";
		window.location.pathname = "/" + $form.elements["h"].value + prefix + encodeURIComponent(query);
		
		ev.preventDefault();
		return false;
	});
	
}

window.addEventListener('load', onLoad);
