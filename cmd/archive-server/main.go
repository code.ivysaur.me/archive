package main

import (
	"encoding/json"
	"flag"
	"io/ioutil"
	"log"
	"net/http"

	"code.ivysaur.me/archive"
)

func main() {
	bindAddr := flag.String("listen", "127.0.0.1:80", "Bind address")
	configPath := flag.String("config", "config.json", "Configuration file")
	flag.Parse()

	//

	cfg, err := ioutil.ReadFile(*configPath)
	if err != nil {
		log.Fatalf("Failed to load configuration file '%s': %s\n", *configPath, err.Error())
	}

	log.Printf("Loading configuration from '%s'...\n", *configPath)
	opts := archive.NewConfig()
	err = json.Unmarshal(cfg, &opts)
	if err != nil {
		log.Fatalf("Failed to parse configuration file: %s\n", err.Error())
	}

	//

	as, err := archive.NewArchiveServer(opts)
	if err != nil {
		log.Fatal(err.Error())
	}

	log.Printf("Starting archive server on '%s'...", *bindAddr)
	err = http.ListenAndServe(*bindAddr, as)
	if err != nil {
		log.Fatal(err.Error())
	}
}
